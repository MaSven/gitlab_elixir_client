defmodule GitlabClient do
  use HTTPoison.Base

  Application.ensure_all_started(:inets)
  Application.ensure_all_started(:ssl)
  @doc"""
  Baseurl used to make requests
  """
  @base_url Application.get_env(:gitlab_client,:base_url,"https://gitlab.com")<>"/api/v4/"

  @gitlab_token Application.get_env(:gitlab_client, :gitlab_token,"")

  @gitlab_project_id Application.get_env(:gitlab_client, :gitlab_project_id, "")

  def process_request_url(url) do
	  @base_url <> url
  end

  def process_response_body(binary) do
	  binary
    |> Poison.decode!()
  end


  def get_all_issues() do
	  get!("projects/#{@gitlab_project_id}/issues",["PRIVATE-TOKEN": "#{@gitlab_token}"]).body
  end


  def write_to_csv(data) do
	  data |> CSV.encode([headers: true])
  end

  def write_all_issues() do
    file = File.open!("gitlab_issues.csv",[:write,:utf8])
    get_all_issues() |> github_josn_to_csv() |> write_to_csv() |> Enum.each(&IO.write(file,&1))
  end

  def github_josn_to_csv(data) when is_list(data), do:
  data |> Enum.map(fn issue ->
	  %{
      "IssueId:" => issue["iid"],
      "URL"=> issue["web_url"],
      "Title"=> issue["title"],
      "State"=> issue["state"],
      "Description"=> issue["description"],
      "Author"=> issue["author"]["name"],
      "Author Username"=> issue["author"]["username"],
      "Assignee"=> issue["assignee"]["name"],
      "Assignee Username"=> issue["assignee"]["username"],
      "Confidental"=> issue["confidental"],
      "Locked"=> issue["discussion_locked"],
      "Due Date"=> issue["due_date"],
      "Created At(UTC)"=> issue["created_at"],
      "Updatet At(UTC)"=> issue["updated_at"],
      "Milestone"=> issue["milestone"],
      "Weight"=> "",
      "Labels"=> issue["labels"] |> Enum.join(","),
      "Time Estimate"=> issue["time_stats"]["time_estimate"],
      "Time Spent"=> issue["time_stats"]["total_time_spent"]
    }
   end )

end
